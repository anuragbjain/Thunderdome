var getstream = require('getstream');
var env = require('../../env.json');

var Stream = function () {
    'use strict';
    var instance;
    function Stream() {
        var object = new getstream.connect(env.getstream.API_KEY, env.getstream.APP_SECRET, env.getstream.APP_ID);
        return object;
    }

    return {
        getConnection: function () {
            if (!instance) {
                instance = new Stream();
            }
            return instance;
        }
    };
};

module.exports = Stream();
