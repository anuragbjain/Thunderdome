var http = require('http');
var request = require('request');

var jobs = {
  "requestRide": {
    plugins: [ 'jobLock' ],
    pluginOptions: {
      jobLock: {},
    },
    perform: function(user_id,ts,callback){
        var b_id = randomizer();
        var activity = {
            "actor":"activity:1",
            "message": "You have scheduled a Uber!",
            "verb":"scheduled",
            "object":"booking_id:"+b_id,
            "foreign_id":"booking_id:"+b_id
        };
        var confirmation = {
            "actor":"activity:1",
            "message": "Your Uber is on its way!",
            "verb":"Accepted",
            "object":"booking_id:"+b_id,
            "foreign_id":"booking_id:"+b_id
        };
        sendPostRequest('http://localhost:3000/v1/stream/1',activity);
        callback(null, sendPostRequest('http://localhost:3000/v1/notification/1',confirmation));
    },
  }
};

function sendPostRequest(url, body){
    request({
        url: url,
        method: "POST",
        json: true,
        body: body
    }, function (error, response){
        if(error){
        console.log(error);
    }
    else{
        console.log(response.statusCode);
    }
});
}

function randomizer() {
  var result =  Math.ceil(Math.random() * 1000);
  return result;
}
module.exports = jobs;
