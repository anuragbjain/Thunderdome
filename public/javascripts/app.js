var app = function(url,notification,stream,transactions,schedule,user_id){
    subscribe(url+notification, user_id);
    pullNotification(url, notification, user_id);
    getTransactionDetails(url+transactions, user_id);
    getActivityFeed(url+stream+user_id, user_id);
    $('.uber_button').on('click',function(){
        var requestType = this.value;
        requestRide(url, stream, notification, schedule, user_id, requestType);
    });
    $('.notification_alert').on('click', function(){
            notificationAlert(url, notification, user_id);
    });
};
