function pullNotification(url, notification, user_id){
    getFeed(url+notification+user_id).done(function(data)
    {
        var elm = $('.list-group');
            notificationAlert(url,notification,user_id);
            if(data.results.length>0){
                $('.notification_alert').html(data.unseen);
                    for(var i = 0; i < data.results.length; i++){
                        elm.append("<a href=\"#\" class=\"list-group-item\">"+
                        "<span class=\"badge\">"+moment(data.results[i].activities[0].time).fromNow()+"</span><i class=\"fa fa-fw "+getActivityType(data.results[i].verb)+ "\">"+
                        "</i> "+data.results[i].activities[0].message+"</a>");
                }
            }
            else{
                elm.append("<p>There is no user activity</p>");
            }
    }).error(
        function(error){
            console.log(error);
    });
}

function notificationAlert(url,notification,user_id) {
    getFeed(url+notification+user_id).done(function(data){
        var dropDownelment = $('.notification-menu').html("");
        if(data.results.length>0){
            $('.notification_alert').html(data.unseen);
                for(var i = 0; i < data.results.length; i++){
                    dropDownelment.append("<li><a href=\"#\">"+data.results[i].activities[0].message+" + "+data.results[i].activity_count+" more</a></li>");
            }
            dropDownelment.append("<li class=\"divider\"></li><li><a href=\"#\" class=\"markRead\">Mark All Read</a></li>");
            $('.markRead').on('click', function(){
                markAllRead(url ,notification, user_id);
            });
        }
        else{
            $('.notification_alert').html("0");
        }
        }).error(function(error){
            // console.log(error);
            $('.notification_alert').removeClass( "label-success");
            $('.notification_alert').addClass( "label-danger");
            $('.notification_alert').html("!");
    });
}

function markAllRead(url,notification,user_id){
    getFeed(url+notification+user_id+"/markAllRead").done(function(data){
            notificationAlert(url,notification,user_id);
        }).error(function(error){
        console.log(error);
    });
}


function getTransactionDetails(url, user_id){
    getFeed(url).done(function(data){
        console.log(data);
        var elm = $('.transaction-table-body');
        if(data.transactions.length > 0){
            for(var i = 0; i < data.transactions.length && i < 15 ; i++){
                elm.append("<tr><td>"+data.transactions[i].pickup+"</td><td>"+data.transactions[i].dropoff+"</td><td> $"+data.transactions[i].amount+"</td><td>"+data.transactions[i].duration+"</td></tr>");
            }
        }
    }).error(function(error){
        console.log(error);
    });
}

function getActivityFeed(url, user_id){
    getFeed(url).done(function(result){
        var elm = $('.activity-feed');
        for(var i = 0; i < result.results.length && i < 15; i++){
            elm.append("<a href=\"#\" class=\"list-group-item\">"+
            "<span class=\"badge\">"+moment(result.results[i].time).fromNow()+"</span><i class=\"fa fa-fw "+getActivityType(result.results[i].verb)+"\">"+
            "</i> "+result.results[i].message+"</a>");
        }
    }).error(function(error){
        console.log(error);
    });

}

function getActivityType(type){
    switch(type){
        case "confirmed":
            return "fa-paper-plane";
        case "Accepted":
            return "fa-thumbs-up";
        case "scheduled":
            return "fa-clock-o";
        case "request":
            return "fa-car";
        default:
            return "fa-car";
    }
}


function getFeed(url){
    return $.ajax({
        type : "get",
        url : url
    }).done(function(data){
    }).error(function(error){
    });
}
