
function requestRide(url, stream, notification, schedule, user_id, requestType)
{
var activity = {
    "actor":"activity:1",
    "message": "Requesting an Uber!",
    "verb":requestType,
    "object":requestType+":31213",
    "foreign_id":requestType+":31212",
};
var confirmation = {
    "actor":"activity:1",
    "message": "Requesting an Uber!",
    "verb":"confirmed",
    "object":requestType+":31213",
    "foreign_id":requestType+":31212",
};


    if(requestType === "schedule"){
        sendRequest(activity, url+schedule);
    }
    else{
        sendRequest(activity, url+stream+user_id);
        sendRequest(confirmation, url+notification+user_id);
    }


function sendRequest(activity, url){
    $.ajax({
        type : "POST",
        url : url,
        data : activity,
        dataType:'json'
    }).done(function(data){
        sendToastNotice(data.verb);
    }).error(function(error)
    {
        toastr.warning('<i class=\"fa fa-fw fa-exclamation-circle\"></i> Uh-Oh! Something is wrong! we are fixing it!');
    });

}

function sendToastNotice(requestType){
    switch(requestType){
        case 'confirmed':
            return toastr.success('<i class=\"fa fa-fw fa-car \"></i> Your Uber is on its way!');
        case 'request':
            return toastr.success('<i class=\"fa fa-fw fa-paper-plane\"></i> Your Uber request has been received!');
        case 'schedule':
            return toastr.info('<i class=\"fa fa-fw fa-clock-o\"></i> Your Uber has been scheduled!');
        default:
            return toastr.success('<i class=\"fa fa-fw fa-paper-plane\"></i> Your Uber request has been received!');
    }
}
}
