var request = require('request');
var sinon = require('sinon');
var server = require('../app.js');
var hippie = require('hippie');

function api() {
    return hippie(server)
    .json().time(true);
}

var endpoint = "/v1/notification/";
var user_id = "testUser";
var activity_id;

var sampleActivity = {
    "actor":"activity:"+user_id,
    "message": "Your Uber is on its way!",
    "verb":"Test",
    "object":"booking_id:1",
    "foreign_id":"booking_id:1"
};

function api() {
    return hippie(server)
    .json();
}
var activity_id;
describe('================================Notification Server test scripts================================', function () {
    describe('get /notification get methods testing', function () {
        it('Checking the token of the test user', function (done) {
            api()
            .get(endpoint+user_id+'/generateToken')
            .expectStatus(200)
            .expectBody({ token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZXNvdXJjZSI6IioiLCJhY3Rpb24iOiJyZWFkIiwiZmVlZF9pZCI6Im5vdGlmaWNhdGlvbnRlc3RVc2VyIn0.5ULrc08RTIJ3CMeHhhdAiz8R99vxksz9fbHEE1wLkdc' })
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should return the notifications for the particular user', function (done) {
            api()
            .get(endpoint+user_id)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
    });
    describe('/notification POST method testing', function () {
        it('it should  successfully add an activity to users activity feed', function (done) {
            api()
            .post(endpoint+user_id)
            .send(sampleActivity)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                activity_id = body.id;
                done();
            });
        });
        it('it should  successfully return the added activity from users activity feed', function (done) {
            api()
            .get(endpoint+user_id+'/activity/'+activity_id)
            .expectStatus(200)
            .end(function(err, res, body) {

                if (err) throw err;
                done();
            });
        });

        it('it should  successfully return an error incomplete activity from users activity feed', function (done) {
            api()
            .post(endpoint+user_id)
            .send()
            .expectStatus(400)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });

    });
    describe('Mark the notifications', function(){
        it('it should  mark all seen should mark all the notifications as seen', function (done) {
            api()
            .get(endpoint+user_id+'/markAllSeen')
            .send()
            .expectStatus(200)
            .expectValue('unseen', 0)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should  mark all read should mark all the notifications as seen and read', function (done) {
            api()
            .get(endpoint+user_id+'/markAllRead')
            .send()
            .expectStatus(200)
            .expectValue('unseen', 0)
            .expectValue('unread', 0)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
    });

});
describe('================================Ending the Notification API Test Script================================',function(){
});
