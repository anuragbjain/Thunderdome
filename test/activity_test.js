var request = require('request');
var sinon = require('sinon');
var server = require('../app.js');
var hippie = require('hippie');
// var props = require('pathval');

var endpoint = "/v1/stream/";
var user_id = "testUser";
var activity_id;

var sampleActivity = {
    "actor":"activity:"+user_id,
    "message": "Your Uber is on its way!",
    "verb":"Test",
    "object":"booking_id:1",
    "foreign_id":"booking_id:1"
};

function api() {
    return hippie(server)
    .json().time(true);
}
describe('================================Activity Server test scripts================================', function () {
    describe('get /stream endpoint testing', function () {
        it('returns a transaction for the user', function (done) {
            api()
            .get(endpoint+user_id)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should  an transaction with limit and offset for the user', function (done) {
            api()
            .get(endpoint+user_id+'?limit=1&offset=1')
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should  an transaction with limit for the user', function (done) {
            api()
            .get(endpoint+user_id+'?limit=1')
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
    });
    describe('/stream POST method testing', function () {
        it('it should  successfully add an activity to users activity feed', function (done) {
            api()
            .post(endpoint+user_id)
            .send(sampleActivity)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                // console.log(body.id);
                activity_id = body.id;
                done();
            });
        });
        it('it should  successfully return the added activity from users activity feed', function (done) {
            api()
            .get(endpoint+user_id+'/activity/'+activity_id)
            .expectStatus(200)
            .end(function(err, res, body) {
                // console.log(body);
                if (err) throw err;
                done();
            });
        });

        it('it should  successfully return an error incomplete activity from users activity feed', function (done) {
            api()
            .post(endpoint+user_id)
            .send()
            .expectStatus(400)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });

    });

describe('Testing the delete functionality of the stream api', function(){
        it('it should  successfully delete the added activity from users activity feed', function (done) {
            api()
            .del(endpoint+user_id+'/activity/'+activity_id)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('deleting an invalid activity should return a 400 error', function (done) {
            var invalid_id;
            api()
            .del(endpoint+user_id+'/activity/'+invalid_id)
            .expectStatus(400)
            .end(function(err, res, body) {
                //console.log(body);
                if (err) throw err;
                done();
            });
        });
        // it('deleting all the activity using foreign id', function (done) {
        //     var foreignid = 'booking_id:1';
        //     api()
        //     .del(endpoint+user_id+'/foreign/'+foreignid)
        //     .expectStatus(200)
        //     .expectValue('results[0].id', undefined)
        //     .end(function(err, res, body) {
        //         if (err) throw err;
        //         done();
        //     });
        // });

    });
});
