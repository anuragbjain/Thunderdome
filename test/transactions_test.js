var request = require('request');
var sinon = require('sinon');
var server = require('../app.js');
var hippie = require('hippie');
function api() {
    return hippie(server)
    .json().time(true);
}

var endpoint = "/v1/transaction/";

var newTransaction = {
    'pickup':'San Diego',
    'dropoff':'San Francisco',
    'amount':40,
    'duration':40,
    'id':10
};
var incompleteTransaction = {
    'dropoff':'San Francisco',
    'amount':20,
    'duration':30
};

describe('================================Testing the transactions api ================================', function () {
    describe('/transactions endpoint', function () {
        it('returns a transaction for the user', function (done) {
            api()
            .get(endpoint)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should  an transaction with id for the user', function (done) {
            api()
            .json()
            .get(endpoint)
            .expectStatus(200)
            .end(function(err, res, body) {
                if (err) throw err;
                done();
            });
        });
        it('it should return an error for wrong api call', function (done) {
            api()
            .get(endpoint)
            .expectStatus(404)
            .end(function(err, res, body) {
                done();
            });
        });
        it('returns a number of transaction for the user for set limit and offset', function (done) {
            api()
            .get(endpoint+'?limit=3&offset=0')
            .expectStatus(200)
            .end(function(err, res, body) {
                if (body.transactions.length === 3 )
                {
                    done();
                }
                else{
                    throw err;
                }
            });
        });
        it('returns a number of transaction for the user for set limit and offset', function (done) {
            api()
            .get(endpoint+'?limit=5')
            .expectStatus(200)
            .end(function(err, res, body) {
                if (body.transactions.length === 5 )
                {
                    done();
                }
                else{
                    throw err;
                }
            });
        });
        it('returns a number of transaction for the user for set limit beyond the data length ', function (done) {
            api()
            .get(endpoint+'?limit=15')
            .expectStatus(200)
            .end(function(err, res, body) {
                if (body.transactions.length === 10 )
                {
                    done();
                }
                else{
                    throw err;
                }
            });
        });
    });
    describe('POST functionality  of the server', function () {
            it('Successfully add user transaction', function (done) {
                api()
                .post(endpoint)
                .send(newTransaction)
                .expectStatus(200)
                .expectBody(newTransaction)
                .end(function(err, res, body) {
                    if (err) throw err;
                    done();
                    console.log(body);
                });
            });
        });
        it('Successfully discard incomplete user transaction', function (done) {
                    api()
                    .post(endpoint)
                    .send(incompleteTransaction)
                    .expectStatus(500)
                    .end(function(err, res, body) {
                        done();
                    });
            });
        it('Successfully discard null body input transaction', function (done) {
                    api()
                    .post(endpoint)
                    .send(null)
                    .expectStatus(400)
                    .end(function(err, res, body) {
                        done();
                    });
                });
        it('Successfully discard undefined body input transaction', function (done) {
                        api()
                        .post(endpoint)
                        .send(undefined)
                        .expectStatus(400)
                        .end(function(err, res, body) {
                            done();
                        });
                });
    });

    describe('PUT functionality  of the server', function () {
        var latestTransaction = {
            'id':4,
            'pickup':'San Diego',
            'dropoff':'San Francisco',
            'amount':20,
            'duration':30
        };

        it('Successfully Update user transaction details and overwrite with the new one', function (done) {
            api()
            .put(endpoint+'/4')
            .send(latestTransaction)
            .expectStatus(200)
            .expectValue('transactions[4]',latestTransaction)
            .end(function(err, res, body) {
                if (err) throw err;
                console.log(body);
                done();
            });
        });
        it('Successfully discard empty user transaction', function (done) {
                    api()
                    .put(endpoint)
                    .send(incompleteTransaction)
                    .expectStatus(500)
                    .end(function(err, res, body) {
                        done();
                    });
            });
    });
    describe('================================Ending the Transactions API Test Script================================',function(){
});
