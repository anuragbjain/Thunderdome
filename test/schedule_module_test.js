var NR = require('node-resque');
var connectionDetails = require('./../lib/scheduler/connection');
var jobs = require('./../lib/scheduler/jobs');
var mocha = require('mocha');
var assert = require('assert');
var chai = require('chai').use(require("chai-as-promised"));
var should = require('chai').should();
var expect = require('chai').expect;
var scheduler = require('./../lib/scheduler/schedule');


describe('========================Testing functionality of the Scheduler Module======================', function() {
    var user_id = 'testUser';
    var ts = Math.floor(Date.now());
    ts = ts+10000;
    var queue = new NR.queue({connection: connectionDetails}, jobs);
    it('Should add a job to the queue',function(){
        queue.connect(function(){
            queue.enqueueAt(ts,'request', 'requestRide',[user_id,ts],
            function(err, result)
            {
                console.log("This is Added");
                if(err){
                    throw err;
                }
            });

        });
  });
  it('Should return the queue a job to the queue',function(){
      queue.connect(function(){
          queue.allDelayed(function(err, hash){
            console.log(hash);
            if(err){
                throw err;
            }
        });

      });
});
});
