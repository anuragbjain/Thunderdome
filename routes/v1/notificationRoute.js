var express = require('express');
var notificationRoute = express.Router();
var stream = require('./../../lib/stream/stream');

notificationRoute.get('/:id/generateToken', function(req, res, next) {
    var _id = req.params.id;
    var token = stream.getConnection().getReadOnlyToken('notification',_id);
    res.status(200).send({ 'token': token});
});

notificationRoute.get('/:id', function(req, res, next) {
    var _id = req.params.id;
    var userNotificationFeed = stream.getConnection().feed('notification',_id);
    userNotificationFeed.get().
    then(function(data){res.send(data);}).
    catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);
        });
    });

notificationRoute.post('/:id',function(req, res, next) {
    var _id = req.params.id;
    var activity = req.body;
    var userFeed = stream.getConnection().feed('notification',_id);
    userFeed.addActivity(activity).then(function(data){res.send(data);}).
        catch(function(reason){
            res.status(reason.error.status_code).send(reason.response);
        });
});

notificationRoute.get('/:id/activity/:activityid', function(req, res, next) {
    var _id = req.params.id;
    var activityid = req.params.activityid;
    var userFeed = stream.getConnection().feed('notification',_id);
    userFeed.get({ limit : 1 , id_lte : activityid}).then(function(data){
        res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});

notificationRoute.delete('/:id/activity/:activityid', function(req, res, next) {
    var _id = req.params.id;
    var activityid = req.params.activityid;
    if(activityid === null || activityid === undefined){ res.status(400).send({'Error': 'invalid activity id'}); }
    var userFeed = stream.getConnection().feed('notification',_id);
    userFeed.removeActivity(activityid).then(function(data){res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});

notificationRoute.delete('/:id/foreign/:foreignid', function(req, res, next) {
    var _id = req.params.id;
    var foreignid = req.params.foreignid;
    if(foreignid === null || foreignid === undefined){ res.status(400).send({'Error': 'invalid foreignid id'}); }
    var userFeed = stream.getConnection().feed('activity',_id);
    userFeed.removeActivity({foreignId: foreignid}).then(function(data){res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});

notificationRoute.get('/:id/markAllRead', function(req, res, next) {
    var _id = req.params.id;
    var userNotificationFeed = stream.getConnection().feed('notification',_id);
    userNotificationFeed.get({mark_seen:true, mark_read:true}).then(function(data){
        res.status(200).send(data);}).catch(function(reason){
            res.status(reason.error.status_code).send(reason.response);});
});

notificationRoute.get('/:id/markAllSeen', function(req, res, next) {
    var _id = req.params.id;
    var userNotificationFeed = stream.getConnection().feed('notification',_id);
    userNotificationFeed.get({mark_seen: true}).then(function(data){
        res.status(200).send(data);
    }).catch(function(reason){
            res.status(reason.error.status_code).send(reason.response);});
});



module.exports = notificationRoute;
