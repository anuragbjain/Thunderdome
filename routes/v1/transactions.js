var express = require('express');
var transactionRoute = express.Router();
var location = ['Airport','La Mesa','Santee','La Jolla','Mira Mesa','Mission Valley','Balboa Park','Downtown','Mission Hills','City Heights','Bankers Hill'];
var data = {};
loadData();
/* GET users listing. */
transactionRoute.get('/', function(req, res, next) {
    var limit = req.query.limit ? parseInt(req.query.limit) : 10;
    var offset = req.query.offset ? parseInt(req.query.offset) : 0;
    if(limit > data.transactions.length)
    { limit = data.transactions.length; }
    var result = {};
    var transactions = [];
    for(var i = offset;i < offset+limit && i < data.transactions.length; i++){
        transactions.push(data.transactions[i]);
    }
    result.transactions = transactions;
    res.status(200).send(result);
});


transactionRoute.post('/', function(req, res, next) {
    var body = req.body;
    body.id = data.transactions.length;
    if(body.hasOwnProperty('pickup')&& body.hasOwnProperty('dropoff') && body.hasOwnProperty('duration') && body.hasOwnProperty('amount')){
        data.transactions.push(body);
        res.status(200).send(body);
    }
    else{
        res.status(404).send({'error':"Missing Arguments",'statusCode':'404'});
    }
    if(body === undefined || body === null)
    {
        res.status(400).send("Bad Request");
    }
});

transactionRoute.put('/:id', function(req, res, next) {
    var _id = parseInt(req.params.id);
    var body = req.body;
    if(data.transactions[_id]){
        data.transactions[_id].id = _id;
        data.transactions[_id].pickup = body.pickup ? body.pickup : data.transactions[_id].pickup ;
        data.transactions[_id].dropoff = body.dropoff ? body.dropoff : data.transactions[_id].dropoff;
        data.transactions[_id].duration = body.duration ? body.duration : data.transactions[_id].duration;
        data.transactions[_id].amount = body.amount ? body.amount : data.transactions[_id].amount;
        res.status(200).send(data);
    }
    else{
        res.status(404).send({'error': 'Wrong id'});
    }
});



transactionRoute.delete('/:id', function(req, res, next) {
    var _id = req.params.id;
    if(data.transactions[_id]){
        var result = data.transactions[_id];
        delete data.transactions[_id];
        res.status(200).send(result);
    }
    else{
        res.status(304).send({'error': 'Element already deleted, id doesnt exits'});
    }
});

transactionRoute.get('/:id', function(req, res, next) {
    var _id = req.params.id;
    if(data.transactions[_id]){
        var result = data.transactions[_id];
        res.status(200).send(result);
    }
    else{
        res.status(304).send({'error': 'id doesnt exits'});
    }
});

function randomizer() {
  var result =  Math.ceil(Math.random() * 10);
  return result;
}


function loadData(){
    var transactions = [];
    for(var i=0;i<10;i++){
        var temp = getUser(i);
        transactions.push(temp);
    }
    data.transactions = transactions;
}
function getUser(i){
    var user = {};
    user.id = i;
    user.pickup = location[randomizer()];
    user.dropoff = location[randomizer()];
    user.duration = 20 + randomizer() * (i+1);
    user.amount = 5.8 + randomizer() * (i+1);
    return user;
}



module.exports = transactionRoute;
