var express = require('express');
var schedule = express.Router();
var stream = require('./../../lib/scheduler/schedule');
var NR = require('node-resque');
var connectionDetails = require('./../../lib/scheduler/connection');
var jobs = require('./../../lib/scheduler/jobs');

schedule.get('/', function(req, res, next) {
    var queue = new NR.queue({connection: connectionDetails}, jobs);
    queue.on('error', function(error){ console.log(error); });
    queue.connect(function(){
        queue.allDelayed(function(err, hash){
          res.status(200).send(hash);
      });
    });

});

schedule.post('/', function(req, res, next) {
    var queue = new NR.queue({connection: connectionDetails}, jobs);
    queue.on('error', function(error){ console.log(error); });
    var body = req.body;
    var user_id = body.user_id;
    queue.connect(function(){
        var ts = Math.floor(Date.now());
        var user_id = 1;
        ts = ts+10000;
        queue.enqueueAt(ts,'request', 'requestRide',[user_id,ts],
        function(err, result)
        {
            console.log("This is Added");
            if(err){
                res.status(500).send(err);
            }
        });

    });
    res.status(200).send({'request':'Success','verb':'schedule'});
});

schedule.put('/', function(req, res, next) {

    res.status(501).send({'Error':'Methods not implemented'});
});

schedule.delete('/', function(req, res, next) {

    res.status(501).send({'Error':'Methods not impelemented'});
});

module.exports = schedule;
