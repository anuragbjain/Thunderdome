var express = require('express');
var streamRoute = express.Router();
var stream = require('./../../lib/stream/stream');

/* GET home page. */

streamRoute.get('/:id', function(req, res, next) {
    var _id = req.params.id;
    var limit = req.query.limit;
    var offset = req.query.offset;
    var userFeed = stream.getConnection().feed('activity',_id);
    userFeed.get({'limit': limit, 'offset':offset }).then(function(data){res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});

streamRoute.post('/:id', function(req, res, next) {
    // res.send(req.body);
    var _id = req.params.id;
    var activity = req.body;
    var userFeed = stream.getConnection().feed('activity', _id);
    userFeed.addActivity(activity).then(function(data){
        res.status(200).send(data);
    }).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});

});

streamRoute.get('/:id/activity/:activityid', function(req, res, next) {
    var _id = req.params.id;
    var activityid = req.params.activityid;
    var userFeed = stream.getConnection().feed('activity',_id);
    userFeed.get({ limit : 1 , id_lte : activityid}).then(function(data){
        res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});


streamRoute.delete('/:id/activity/:activityid', function(req, res, next) {
    var _id = req.params.id;
    var activityid = req.params.activityid;
    if(activityid === null || activityid === undefined){ res.status(400).send({'Error': 'invalid activity id'}); }
    var userFeed = stream.getConnection().feed('activity',_id);
    userFeed.removeActivity(activityid).then(function(data){res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});

streamRoute.delete('/:id/foreign/:foreignid', function(req, res, next) {
    var _id = req.params.id;
    var foreignid = req.params.foreignid;
    if(foreignid === null || foreignid === undefined){ res.status(400).send({'Error': 'invalid foreignid id'}); }
    var userFeed = stream.getConnection().feed('activity',_id);
    userFeed.removeActivity({foreignId: foreignid}).then(function(data){res.status(200).send(data);}).catch(function(reason){
        res.status(reason.error.status_code).send(reason.response);});
});



 ///// Alll the invalid methods ///

streamRoute.all('/', function(req, res, next)
{
    invalidMethod(res);
});

streamRoute.put('/:id', function(req, res, next) {
    invalidMethod(res);
});

streamRoute.delete('/:id', function(req, res, next) {
    invalidMethod(res);
});


streamRoute.post('/:id/activity/:activityid', function(req, res, next) {
    invalidMethod(res);
});

streamRoute.post('/:id/activity/:activityid', function(req, res, next) {
    invalidMethod(res);
});

function invalidMethod(res){
    return res.status(501).send({'Error':'Methods not implemented'});

}

module.exports = streamRoute;
