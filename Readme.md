Hello,

This is my submission for the Uber coding challenge.
I have worked on a custom solution, based on my past experience with some of the stream and notification API (Getstream.io) and Node-rescue for a beta feature of scheduling a task for a particular time in future. In this case i have worked on a feature which allows user to book or request an Uber after a delay of 2 minutes. Every event generates an event on the activity feed and a notification.

Prerequisite: Node js, npm (Node Packa Manager) and working internet connection.

Steps to launch the app:

1. Clone the project
2. cd Thunderdome
3. npm install (if you have node installed in your machine, or else you'll have to install node first)
4. npm start

Go to your browser and go to address 'http://localhost:3000'

I have used an express generator to generate the basic structure of the Express server, and all the routers and test scripts are written by me, including the js wrappers for getstream and scheduler in the lib folder.

On the front end, i have used bootstrap template and modified it to meet the requirements. All the jquery events are written by me.  

There are two components to the application:

1.Frontend: I have used a standard bootstrap template (http://startbootstrap.com/template-overviews/sb-admin-2/) and customized it according to match my mock-up.
    a. When the application is launched on the browser, user is taken to a dashboard which load users activity,     notification feeds, and transactions history (all are pulling data from 3 different api's in the back end).
    b. There are two buttons in one for requesting an Uber, and another one is for Scheduling an uber after a certain time delay.
    c. Everytime the button is clicked, there is a notification sent to the user, Which is visible as unread notification number on the right hand top corner of the page. (bell icon). When clicked on the notification icon, it pulls down the list of the users notification feed, with an option to mark all the options as read, which changes the unread number to zero. (all via API calls to the getstream server which handles the notification)
    d. The whole site is responsive and works well on mobile.

2. Backend: The backend api consists of a set of 4 different API's
    1. Stream or Activity API
    2. Transactions API
    3. Notifications API
    4. Scheduler API (Beta)

    All the methods available are based on the functionality implemented in the app, there are few addtional methods available too, but not used in the app.

    I have tried to implement the API's using 3 different approaches.
        The Stream/Activity and Notification API uses a third party service getstream.io and its written like a wrapper around the service.

        End point : /v1/Stream
        Methods available :
            GET : /:id  - To get all the user activities
            POST : /:id  - To add an activity to User's feed
            GET : /:id/activity/:activityid - TO get a particular activity by activity id
            DELETE : /:id/activity/:activityid - TO delete an activity by user id and activity id
            Delete : /:id/foreign/:foreignid - to delete by by user id and foreign ID

        End Point : v1/notification
        Methods Available:
            GET : /:id/generateToke - TO get the Read only token for the realtime notifications feed.
            GET : /:id  - To get all the user activities
            POST : /:id  - To add an activity to User's feed
            GET : /:id/activity/:activityid - TO get a particular activity by activity id
            DELETE : /:id/activity/:activityid - TO delete an activity by user id and activity id
            Delete : /:id/foreign/:foreignid - to delete by by user id and foreign ID
            GET : /:id/markAllRead - TO mark all the notifications read and seen.
            GET : /:id/markAllSeen - TO mark all the notifications as just seen.

        The Scheduler uses a third party library Node-Resque and utilizes its scheduler and worker to implement a task scheduler so that it can be used to schedule events and method calls to the stream and notifications api. (I have used both these services in the past, and in the hindsight i feel that i should not have used scheduler for this project as it was difficult to test my implementation using unit and integration test and there are not enough unit tests for this feature. But from the front end side its been working, i felt confident in using the module as its been unit tested, i felt short on the integration tests).

        End Point : v1/schedule

        Methods Available:
            GET : /:id TO get a list of all the jobs scheduled
            POST: /:id TO schedule a job in future to request an uber.

        The transactions API is a simple REST api which implements all the standard rest methods and structure. the data is in memory, and every time the app is launched, the data is pushed randomly into the array.
            End Point : v1/transactions
            GET: / TO get all the transactions for the user
            GET: /:id To ger the transaction with the particular id
            POST: / To post the users transaction and store it in the database
            PUT: /:id TO update the transaction of the particular id
            DELETE: /:id To delete the transaction associated with the id

        Testing: I have tested the API end points using Mocha and Hippie, All the test scripts are in the folder test. The naming convention for each script is 'API Endpoint'+test .js, and for module its endpoint+module .js. There can never be enough testing, the tests are written in such a ways that its readable and explain what is being tested.

        Command to run tests : mocha test or npm test.

        I feel that i felt short on time for testing, There can be more integration tests and given more time i'll definitely like to refactor the code to make it more testable, especially rewrite the Scheduler api.If given more time i would have also liked to do front end testing.
